%macro escribir 2
        mov eax, 4
        mov ebx, 1
        mov ecx, %1
        mov edx, %2
        int 80h
%endmacro

segment .data
    msj1 db "Ingresa datos en el archivo", 10
    len_msj1 equ $-msj1

    archivo db "/home/bryan/Escritorio/ensam/ensamblador/clase/archivo.txt"

segment .bss
    texto resb 30
    idarchivo resb 1

segment .txt    
    global _start

leer:
    mov eax, 3
    mov ebx, 0
    mov ecx, texto
    mov edx, 10;
    int 80h
    ret

_start:
    mov eax, 8   ; servicio para crear archivos, trabajar con archivos
    mov ebx, archivo ;direccion del archivo
    mov ecx, 1    ; MODO DE ACCESO
                  ; 0-RDONLY 0: El archivo se abre solo para leer
                  ; 0-WRONLY 1: El archivo se abre para escritura
                  ; 0-RDWR 2: El archivo se abre para la escritura y lectura
                  ; 0-CREATE 256: crea el archivo en caso de que no exista
                  ; 0-APPEND 2000h: El archivo se abre solo para escritura final
    mov edx, 777h
    int 80h
    
    test eax, eax

    jz salir
    
    mov dword [idarchivo], eax
    escribir msj1, len_msj1

    

    call leer
    ; escritura
    mov eax, 4
    mov ebx, [idarchivo]
    mov ecx, texto
    mov edx, 20
    int 0x80


salir:
    mov eax,1
    int 80h