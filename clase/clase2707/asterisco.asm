; programa que imprime *********
section .data
    asterisco db '*'
section .text
        global _start
start:
    mov cx, 9
imprimir:
    dec cx
    push cx
    mov ax, 4
    mov bx, 1
    mov cx, asterisco
    mov dx, 1
    int 80h
    cmp cx, 0
    jnz imprimir
    pop cx
    jmp imprimir

    mov ax, 1
    int 80h