section .data
        nombres DB "BRYAN ALBERTO",10; constante mensaje de un byte en memoria
        apellidos DB " REQUENES TROYA",10 ; constante mensaje de un byte en memoria
        materia DB "  ENSAMBLADOR",10 ; constante mensaje de un byte en memoria
        genero DB "  MASCULINO",10 ; constante mensaje de un byte en memoria

        longitud_nombre EQU $-nombres              ; constante longuitud que calcula el el # caracteres del mensaje
        longitud_apellido EQU $-apellidos
        longitud_materia EQU $-materia
        longitud_genero EQU $-genero
section .text
        global_start
_start:
        ; ***************imprimir el mensaje****************
        mov eax, 4        ; tipo de subrutina, operacion => escritura,salida
        mov ebx, 1        ; tipo de estandar, por el teclado (1)
        mov ecx, nombres  ; el registro ecx se almacena la referecia a imprimir "mensaje"
        mov edx, longitud_nombre ; el registro ecx se almacena la referecia a imprimir por # caracteres
        
        int 80H           ; interrupcion de sofware para el SO, linux

        mov eax, 1        ; salida del promgrama, teminacion de programa
        mov ebx, 0        ; si el retorno es 0 (200 en la web) OKK
        int 80H
