;BRYAN REQUENES
%macro escribir 2
  mov eax, 4
  mov ebx, 1
  mov ecx, %1
  mov edx, %2
  int 80h
%endmacro


section .data
    newLine db '',10
    mjsMul db ' * '
    lenMul equ $-mjsMul
    msjIgual db ' = '
section .bss
    n resb 2
    dlIn resb 2
    r resb 2
section .text
    global _start
_start:

    mov al, 1
valoresDefecto:
    mov dl,0
    add al, '0'
    mov [n], al
    sub al, '0'
    push ax

multiplicar:
    inc dl
    push ax
    push dx
    mul dl

    ;convertir a 2 valores
    mov bl,10
    div bl
    add eax, '00'
    mov [r], eax

    ;Obtener el valor del incremento
    mov ax,dx
    div bl
    add ax, '00'
    mov [dlIn], ax
    
    
    
    call escribir_numero; escribir n,1
    escribir mjsMul,lenMul
    escribir dlIn,2
    escribir msjIgual,3
    call escribir_numero2; escribir r, 2
    escribir newLine,1

    pop dx
    pop ax
    cmp dl,9
    je comparacion
    jmp multiplicar

comparacion:
    escribir newLine,1
    pop ax
    inc al
    cmp al,10
    je salir
    jmp valoresDefecto
escribir_numero:
    mov eax, 4
    mov ebx, 1
    mov ecx, n
    mov edx, 1
    int 80h
    ret
escribir_numero2:
    mov eax, 4
    mov ebx, 1
    mov ecx, r
    mov edx, 2
    int 80h
    ret
salir:
    mov eax, 1
    int 80h
