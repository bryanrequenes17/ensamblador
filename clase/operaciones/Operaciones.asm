; Bryan Requenes

%macro ingreso 2
    mov eax, 4 
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80H
%endmacro
section.data
    suma db 'resultado suma :',10,13
    len_s EQU $ -suma
    resta db 10,13,'resultado resta:',10,13
    len_r EQU $ - resta 
    multi db 'resultado m:',10,13
    len_m EQU $ - multi
    divi db resultado d:',10,13
    len_d EQU $ - divi

section .bss
    res resb 1
    m resb 2 
    cociente resb 2 
    residuo resb 2 
section .text
    global _start
_start
    ;SUMA
    ingreso suma, len_s
    mov eax,4
    mov ebx,2
    add eax,ebx
    add eax, '0'
    mov[res],eax
    ingreso res, 3
    ;RESTA
    ingreso resta, len_r
    mov eax, 4
    mov ebx, 2
    sub eax,ebx
    add eax,'0'
    mov [res],eax
    ingreso res, 3
    ;MULTIPLICACION
    mov eax, 4
    mov ebx, 2
    mul ebx
    add eax, '0'
    mov[m],eax
    ingreso m, 3
    ;DIVISION
    mov eax,4
    mov ebx,2
    div ebx
    mov [cociente],eax
    
