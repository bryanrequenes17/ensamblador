; Bryan Requenes
section .data 
    mensaje db "El resultado es: ",
    leng equ $-mensaje
    espacio db "",10
    lengEspacio equ $-espacio
section .bss
    resta resb 1   ;Directiva de datos, dos tipos
    
section .text
    global _start
_start:


    ;******RESTA********
    mov eax, 9
    mov ebx, 9
    sub eax, ebx
    add eax, '0' ;tranformar de numero a cadena
    mov [resta], eax 

    ;******MENSAJE********
    mov eax, 4
    mov ebx, 1
    mov ecx, mensaje
    mov edx, leng
    int 80h
    ;******PRESENTACION RESTA********
    mov eax, 4
    mov ebx, 1
    mov ecx, resta
    mov edx, 1
    int 80h
    ;******MENSAJE********
    mov eax, 4
    mov ebx, 1
    mov ecx, espacio
    mov edx, lengEspacio
    int 80h

    mov eax, 1
    int 80h