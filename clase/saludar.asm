section .data
        mensaje DB "Mi primera vez con NASM" ; constante mensaje de un byte en memoria
        longitud EQU $-mensaje               ; constante longuitud que calcula el el # caracteres del mensaje

section .text
        global_start
_start:
       
        mov eax, 4        ; tipo de subrutina, operacion => escritura,salida
        mov ebx, 1        ; tipo de estandar, por el teclado (1)
        mov ecx, mensaje  ; el registro ecx se almacena la referecia a imprimir "mensaje"
        mov edx, longitud ; el registro ecx se almacena la referecia a imprimir por # caracteres
        int 80H           ; interrupcion de sofware para el SO, linux

        mov eax, 1        ; salida del promgrama, teminacion de programa
        mov ebx, 0        ; si el retorno es 0 (200 en la web) OKK
        int 80H