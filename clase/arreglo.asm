segment .data
	arreglo db 3,2,0,7,5
	len_arreglo equ $-arreglo
segment .bss

segment .text
	global _start
	
_start:
	mov al, [esi]
	add al, '0'
	mov esi, arreglo   ;esi = fijar el tamanio del arreglo, posicionar el arreglo en memoria
	mov edi, 0         ;edi = contener el indice del arreglo
	
imprimir:
	cmp edi, len_arreglo  ;cmp 3, 4=> se activa carry cuando el primer operando es menor
						   ;cmp 4,3 => desactiva carry y zero se activa cuando el primer operando es mayor
	jmp imprimir       ; se ejecuta la bandera de carry esta activada
	
salir: 
	mov eax, 1
	int 0x80
