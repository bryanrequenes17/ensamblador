%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data

    saludo db ":  Hola",10
	lenSaludo equ $-saludo

    mensaje db 10,"numero "
	lenMensaje equ $-mensaje

section .bss
    item resb 2

section .text
    global _start
_start:
    mov ecx,30
ciclo:
    push ecx
    mov al, cl
    mov cl, 10
    div cl
    add ax, "00"
    mov [item], eax
    imprimir mensaje, lenMensaje
    imprimir item, 2
    imprimir saludo,lenSaludo
    pop ecx
    loop ciclo

salir:
    mov eax, 1
    int 80h
