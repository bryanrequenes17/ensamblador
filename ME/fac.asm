section    .data
msg db 'El factorial es:',0xa
len equ $ - msg

msg2 db 'Ingrese un numero:',0xa
len2 equ $ - msg2

section .bss
fact resb 1
numero resb 1
section .text
   global _start         ;

_start:                  ;

   mov      edx,len2        ;
   mov      ecx,msg2        ;
   mov      ebx,1          ;
   mov      eax,4          ;
   int      0x80           ;

   mov    eax,3           ;
   mov      ebx,2           ;
   mov      ecx,numero          ;
   mov      edx,2          ;
   int      0x80           ;
	

  
   mov bx, [numero]          ;
   sub bx, '0'
   
   call  proc_fact
   add   ax, 30h
   mov  [fact], ax

   mov      edx,len        ;
   mov      ecx,msg        ;
   mov      ebx,1          ;
   mov      eax,4          ;
   int      0x80           ;

   mov   edx,1           ;
   mov      ecx,fact       ;
   mov      ebx,1          ;
   mov      eax,4          ;
   int      0x80           ;

   mov      eax,1          ;
   int      0x80           ;

proc_fact:
   cmp   bl, 1
   jg    do_calculation
   mov   ax, 1
   ret

do_calculation:
   dec   bl
   call  proc_fact
   inc   bl
   mul   bl        ;ax = al * bl
   ret
